import java.util.Iterator;
import java.util.LinkedList;

/**
 * Algoritmen kan beskrivas enligt följande: Börja med startnoden Om vi når
 * väggen eller noden redan är besökt, hoppa till nästa "iteration" Om
 * utgångsnoden nås, backspår från aktuell nod till startnoden för att hitta den
 * kortaste sökvägen
 * 
 * Annars, lägg till alla omedelbara grannar i de fyra riktningarna En viktig
 * sak här är att noderna måste hålla reda på sin förälder, dvs varifrån de
 * lades till i kön. Detta är viktigt för att hitta sökvägen när utgångsnoden
 * påträffas.
 * 
 * Denna klass räknar också exekveringstid tills att hitta slut-punkt samt antal
 * steg.
 * 
 * 
 * @author Wissam Rashed
 * @author Rawand Alkilani
 * @version 2021-01-09
 * @version 2006-01-09
 *
 */

public class Solver_BFS extends Solver_abstract {
	public Solver_BFS(Maze maze) {

		this.linkedNode = new LinkedList<Node<Maze>>();
		this.closedCell = new LinkedList<Cell>();
		this.maze = maze;
		this.result = "";
	}

	public String solver() {
		try {
			Boolean endIsFound = false;
			this.countNode = 0;
			this.pathSteps = 0;

			this.closedCell.clear();
			this.maze.startSolveMaze();

			this.linkedNode.clear();
			this.linkedNode.add(new Node<Maze>(this.maze));

			// räkna exekveringstid
			long startTime = System.currentTimeMillis();

			// algoritm som hittar huvudlösning
			while (!endIsFound) {
				if (this.linkedNode.isEmpty())
					break;

				else {
					Node<Maze> currentNode = ((LinkedList<Node<Maze>>) this.linkedNode).removeFirst();

					this.maze = (Maze) currentNode.getContent();
					Cell currState = this.maze.getCurrCell();

					if (currState.getLine() == this.maze.getEnd().getLine()
							&& currState.getCol() == this.maze.getEnd().getCol()) {
						Node<Maze> temp = new Node<Maze>(this.maze);
						temp.setParent(currentNode);
						this.linkedNode.add(temp);
						endIsFound = true;
					}

					else {
						LinkedList<Node<Maze>> nexts = this.getNextCell();
						if (!this.closedCell.contains(currState)) {
							this.closedCell.add(currState);
							currState.setCell("*");
						}

						Iterator<Node<Maze>> x = nexts.iterator();

						while (x.hasNext()) {
							Node<Maze> temp = x.next();
							temp.setParent(currentNode);
							this.linkedNode.add(temp);
							this.countNode++;
						}
					}

				}
			}

			long endTime = System.currentTimeMillis();

			long time = endTime - startTime;

			this.result = "Breadth-first search ";

			if (endIsFound) {
				this.maze.resetCell();
				Node<Maze> revertedTree = ((LinkedList<Node<Maze>>) this.linkedNode).removeLast();

				revertedTree = revertedTree.getParent();
				this.result += "Path: [" + this.maze.getEnd().toString() + "](End) && ";
				this.pathSteps++;

				while (revertedTree.hasParent()) {
					Maze temp = revertedTree.getContent();
					Cell state = temp.getCurrCell();

					if (!state.equals(this.maze.getEnd())) {
						this.maze.getMatrixCell()[state.getLine()][state.getCol()].setCell("*");
						this.pathSteps++;
					}
					revertedTree = revertedTree.getParent();
				}

				this.result += "[" + this.maze.getStart().toString() + "]" + "(Start) \n" + "Path length: "
						+ this.pathSteps + "\nNumber of nodes created: " + this.countNode + "\nExecution time: "
						+ time / 1000d + " seconds\n";
				this.result += this.maze.printMaze();
			} else {
				this.result += "Failed : The maze is unreachable.";
			}
		} catch (Exception e) {

			return "Fail File not found";
		}
		return this.result;
	}

	// flytta till en ny cell med hjälp av noder
	public LinkedList<Node<Maze>> getNextCell() {
		LinkedList<Node<Maze>> currentResult = new LinkedList<Node<Maze>>();

		// hämta 4 möjliga cell
		LinkedList<Maze> nexts = this.maze.getCurrCell().getNextCell();

		for (int i = 0; i < nexts.size(); i++) {
			Cell tempSq = nexts.get(i).getCurrCell();
			if (!this.closedCell.contains(tempSq)) {

				Node<Maze> tempNode = new Node<Maze>(nexts.get(i));
				currentResult.add(tempNode); // Add the state
			}
		}

		return currentResult;
	}

	public String getResult() {
		return this.result;
	}

}
