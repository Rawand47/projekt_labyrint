import java.util.AbstractCollection;
import java.util.LinkedList;

/**
 * 
 * Lösa labyrinten med hjälp av (BFS, DFS) algoritm och hitta resultaten genom att: 
 *  1 Spår av banan
 *  2 Steglängd 
 *  3 Antal noder som skapats 
 *  4 Labyrinten med skriven väg
 * 
 * @author Wissam Rashed
 * @author Rawand Alkilani
 * @version 2021-01-09
 * @version 2006-01-09
 *
 */
public abstract class Solver_abstract {
	protected Maze maze;
	protected String result;
	protected AbstractCollection<Node<Maze>> linkedNode;
	protected AbstractCollection<Cell> closedCell;
	protected int countNode;
	protected int pathSteps;

	public abstract String solver();

	/*
	 * hämta och möjligtvis flytta till nästa cell
	 */
	public abstract LinkedList<Node<Maze>> getNextCell();

	/*
	 * returnera resultat av lösningen
	 */
	public abstract String getResult();

}
