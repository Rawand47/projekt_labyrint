import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class JUnit_Test {
	@Test
	void test_BFS_wrongFileName() throws IOException {
		Maze lab2 = new Maze("./data/not_excited_file.txt");
		Solver_BFS BFS = new Solver_BFS(lab2);
		assertEquals(BFS.solver(), "Fail File not found");
	}
	@Test
	void test_DFS_wrongFileName() throws IOException {
		Maze lab2 = new Maze("./data/not_excited_file.txt");
		Solver_DFS DFS = new Solver_DFS(lab2);
		assertEquals(DFS.solver(), "Fail File not found");
	}
	@Test
	void test_DFS_rightFile() throws IOException {
		Maze lab2 = new Maze("./data/maze.txt");
		Solver_DFS DFS = new Solver_DFS(lab2);
		assertDoesNotThrow(() -> DFS.solver());
	}
	@Test
	void test_BFS_rightFile() throws IOException {
		Maze lab2 = new Maze("./data/maze.txt");
		Solver_BFS BFS = new Solver_BFS(lab2);
		assertDoesNotThrow(() -> BFS.solver());
	}
	@Test
	void test_SetSizeToMaze() throws IOException {
		RandomMazeFile fileSize = new RandomMazeFile();
		assertDoesNotThrow(() -> fileSize.setSize(3));
		assertEquals(fileSize.setSize(-3), "fail size");
	}
}