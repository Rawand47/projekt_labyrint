/**
 * Denna klass kopplar celler med varandra genom Noder för att sätta värdet på
 * noden och koppla nuvarande Noden med Noden som äger den.
 * 
 * 
 * @author Wissam Rashed
 * @author Rawand Alkilani
 * @version 2021-01-09
 * @version 2006-01-09
 * 
 */

public class Node<T> {
	private T value;
	private Node<T> föräldrar;
	 //Constructor att koppla noder med varandra
	public Node(T t) {
		this.value = t;
		this.föräldrar = null;
	}
	 //Retunera noden 
	public T getContent() {
		return value;
	}
	//sätta värdet på noden 
	public void setValue(T value) {
		this.value = value;
	}
	// retunera färäldrar node
	public Node<T> getParent() {
		return föräldrar;
	}
	// Sätta värdert på föräldrar 
	public void setParent(Node<T> parent) {
		this.föräldrar = parent;
	}
	//controlera om noden har förälderrar
	public boolean hasParent() {
		return this.föräldrar != null;
	}
	//Retunerar värdet på noden och föräldrar 
	public String printParent() {
		return this.value.toString() + " && " + this.föräldrar.value.toString();
	}
	public String toString() {
		return this.getContent().toString();
	}
}
