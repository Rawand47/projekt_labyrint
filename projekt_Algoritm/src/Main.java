import java.io.IOException;
import java.util.Scanner;

/**
 * Denna klass kör projektet, och ber användare att ange storlek på labyrint.
 * 
 * @author Wissam Rashed
 * @author Rawand Alkilani
 * @version 2021-01-09
 * @version 2006-01-09
 * 
 */

public class Main {
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the maze:");
		int size = sc.nextInt();
		while (size <= 2) {
			System.out.println("Enter the size of the maze, the size must be more then 2:");
			size = sc.nextInt();
		}
		sc.close();

		RandomMazeFile RandomMazeFile = new RandomMazeFile();
		RandomMazeFile.setSize(size);

	}
}
