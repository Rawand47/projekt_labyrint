import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import javax.annotation.processing.FilerException;

/**
 * Genererar slumpmässig labyrint med en storlek som bestäms av användare och
 * spara den i en fil för att senare ska läsas av andra klasser.
 * 
 * Den första cellen är start-punkten och den sista är slut-punkten.
 * 
 * @author Wissam Rashed
 * @author Rawand Alkilani
 * @version 2021-01-09
 * @version 2006-01-09
 *
 */

public class RandomMazeFile {

	public String setSize(int size) throws IOException {
		if (size <= 2) {
			return "fail size";
		}
		String maze = "";
		try {
			Random r = new Random();
			BufferedWriter buffer = null;
			FileWriter writer = new FileWriter(new File("./data/maze.txt"));
			buffer = new BufferedWriter(writer);
			// definierar start med 'S'
			buffer.write("SO");
			maze += "SO";
			// X betyder vägg och O betyder väg
			String alphabet = "XOO";
			for (int a = 0; a < size - 2; a++) {
				// här fyllas maze med random 'X' eller 'O'
				char c = alphabet.charAt(r.nextInt(alphabet.length()));
				buffer.write(c);
				maze += c;
			}
			buffer.write("\n");
			maze += "\n";
			for (int i = 1; i < size - 1; i++) {
				for (int j = 0; j < size; j++) {
					char c = alphabet.charAt(r.nextInt(alphabet.length()));
					buffer.write(c);
					maze += c;
				}
				buffer.write("\n");
				maze += "\n";

			}

			for (int a = 0; a < size - 2; a++) {
				char c = alphabet.charAt(r.nextInt(alphabet.length()));
				buffer.write(c);
				maze += c;
			}
			// definierar slut eller målet
			buffer.write("OE");
			maze += "OE";
			buffer.close();

		} catch (FilerException e1) {

		}
		// här finns två filer en random fil och en redan bestämt fil för testet vid
		// behövs

		// Maze lab2 = new Maze("./data/lab.txt"); //en redan bestämt fil för testet vid
		// behövs

		Maze lab2 = new Maze("./data/maze.txt"); // random fil

		Solver_BFS BFS = new Solver_BFS(lab2);
		Solver_DFS DFS = new Solver_DFS(lab2);

		if (BFS.solver().equals("Breadth-first search Failed : The maze is unreachable.")) {
			maze = "";
			setSize(size);

		} else {
			System.out.println("the maze shown below, where 'O' is path and 'X' is wall:");
			System.out.println(maze);
			System.out.println("the first solve and the shortest is:");
			System.out.println(BFS.solver());
			System.out.println("the second solve is:");
			System.out.println(DFS.solver());
		}
		return "";
	}

}
