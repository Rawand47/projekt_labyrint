import java.util.LinkedList;

/**
 *
 * Denna klass representerar celler som tillsammans gör en labyrint. varje cell
 * har en position som bestäms av rad och kolumn nummer. klassen används också
 * för att kolla andra celler som går att flytta till samt att kolla om cellen
 * är en vägg eller en väg.
 * 
 * @author Wissam Rashed
 * @author Rawand Alkilani
 * @version 2021-01-09
 * @version 2006-01-09
 * 
 */

public class Cell {
	private Maze maze;
	private String cell;
	private boolean wall;
	private int column;
	private int line;

	/*
	 * Konstruktör för cell i labyrinten med S som startposition och E som
	 * Slutposition.
	 */
	public Cell(int line, int column, String cell) {
		this.line = line;
		this.column = column;
		this.cell = cell; // S = Start; E = End
		this.wall = false;

	}

	/*
	 * Konstruktör för en väggcell i labyrinten. Ställ in som True för att definiera
	 * cell som en vägg
	 */
	public Cell(int line, int column, boolean wall) {
		this.line = line;
		this.column = column;
		this.cell = null;
		this.wall = wall;

	}

	public void defineMaze(Maze maze) {
		this.maze = maze;
	}

	public int getLine() {
		return line;
	}

	public int getCol() {
		return column;
	}

	/*
	 * för att fltta till en ny cell i maze
	 */
	public LinkedList<Maze> getNextCell() {
		LinkedList<Maze> nexts = new LinkedList<Maze>();

		for (int i = 0; i < 4; i++) {
			Maze tempMaze = this.maze.clone();

			if (this.maze.moveDirection[i] == 'N') {
				if (this.getNorth() != null && !this.getNorth().checkWall()) {
					tempMaze.setNextCell(this.getNorth());
					nexts.push(tempMaze);
				}
			} else if (this.maze.moveDirection[i] == 'E') {
				if (this.getEast() != null && !this.getEast().checkWall()) {
					tempMaze.setNextCell(this.getEast());
					nexts.push(tempMaze);
				}
			} else if (this.maze.moveDirection[i] == 'S') {
				if (this.getSouth() != null && !this.getSouth().checkWall()) {
					tempMaze.setNextCell(this.getSouth());
					nexts.push(tempMaze);
				}
			} else if (this.maze.moveDirection[i] == 'W') {
				if (this.getWest() != null && !this.getWest().checkWall()) {
					tempMaze.setNextCell(this.getWest());
					nexts.push(tempMaze);
				}
			}
		}
		return nexts;
	}

	/*
	 * kontrollera att det går att gå uppåt
	 */
	public Cell getNorth() {
		if (this.line - 1 < 0)
			return null;
		else
			return this.maze.getMatrixCell()[this.line - 1][this.column];
	}

	/*
	 * kontrollera att det går att gå nedåt
	 */
	public Cell getWest() {
		if (this.column - 1 < 0)
			return null;
		else
			return this.maze.getMatrixCell()[this.line][this.column - 1];
	}

	/*
	 * kontrollera att det är möjligt att gå till vänster
	 */
	public Cell getSouth() {
		if (this.line + 1 == this.maze.maxLine)
			return null;
		else
			return this.maze.getMatrixCell()[this.line + 1][this.column];
	}

	/*
	 * kontrollera att det är möjligt att gå till höger
	 */
	public Cell getEast() {
		if (this.column + 1 == this.maze.maxColumn)
			return null;
		else
			return this.maze.getMatrixCell()[this.line][this.column + 1];
	}

	public String getCell() {
		return cell;
	}

	public void setCell(String attribute) {
		if (attribute == " " || attribute == "S" || attribute == "E" || attribute == "*") {
			this.cell = attribute;
			this.wall = false;
		}
	}

	public boolean checkWall() {
		return this.wall;
	}

	public String toString() {
		return this.line + ", " + this.column;
	}
}
