import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Denna klass representerar labyrint och bestämmer start och slut punkten samt
 * antal rader och kolumner. Detta går genom att läsa en slumpmässigt genererade
 * textfil som representerar labyrint.
 * 
 * När filen läsas då bestäms varje cell om den är start punkt, slut punkt, vägg
 * eller väg. I slutet kommer klassen att printa labyrint till användare.
 * 
 * 
 * 
 * @author Wissam Rashed
 * @author Rawand Alkilani
 * @version 2021-01-09
 * @version 2006-01-09
 * 
 */

public class Maze {
	private Cell[][] matrixCell;
	private Cell start;
	private Cell end;
	private Cell currCell;
	public int maxLine;
	public int maxColumn;
	public char[] moveDirection = { 'N', 'E', 'S', 'W' };

	/*
	 * Konstruktör med fil som representeras random maze, random maze genereras i
	 * klassen "RandomMazeFile"
	 */
	public Maze(String myfile) throws IOException {
		try {
			BufferedReader file = new BufferedReader(new FileReader(myfile));
			String line = file.readLine();
			String buffer = line.toUpperCase();
			int counter = 0;

			this.maxColumn = line.length();
			this.maxLine = 1;

			while ((line = file.readLine()) != null) {
				this.maxLine++;
				buffer += line.toUpperCase();
			}

			this.matrixCell = new Cell[this.maxLine][this.maxColumn];

			for (int i = 0; i < this.maxLine; i++) {
				for (int j = 0; j < this.maxColumn; j++) {
					switch (buffer.charAt(counter++)) {
					default:
						this.matrixCell[i][j] = new Cell(i, j, " ");
						break;

					case 'S':
						this.setStart(new Cell(i, j, "S"));
						break;

					case 'E':
						this.setEnd(new Cell(i, j, "E"));
						break;

					case 'X':
						this.matrixCell[i][j] = new Cell(i, j, true);
						break;

					}
				}
			}

			file.close();

			this.makeTheMazeIntoCell();

			this.currCell = this.getStart();
		} catch (FileNotFoundException e) {
			System.out.println("Fail");
		}
	}

	public Maze(Cell[][] gCell, Cell start, Cell end, Cell currState, int maxLine, int maxColumn) {
		this.maxLine = maxLine;
		this.maxColumn = maxColumn;
		this.start = start;
		this.end = end;
		this.currCell = currState;
		this.matrixCell = gCell;

	}

	public Cell getStart() {
		return start;
	}

	public void setStart(Cell start) {
		this.start = start;
		this.matrixCell[start.getLine()][start.getCol()] = start;
	}

	public Cell getEnd() {
		return end;
	}

	public void setEnd(Cell end) {
		this.end = end;
		this.matrixCell[end.getLine()][end.getCol()] = end;
	}

	public Cell getCurrCell() {
		return this.currCell;
	}

	public void setCurrCell(Cell c) {
		this.currCell = c;
	}

	public void setNextCell(Cell c) {
		this.matrixCell[this.currCell.getLine()][this.currCell.getCol()].setCell("*");
		this.currCell = c;
	}

	public void makeTheMazeIntoCell() {
		for (int i = 0; i < this.maxLine; i++) {
			for (int j = 0; j < this.maxColumn; j++) {
				this.matrixCell[i][j].defineMaze(this);
			}
		}
	}

	/*
	 * används vid lösning av solve algoritmer
	 */
	public void startSolveMaze() {
		// Init grid
		this.resetCell();

		this.currCell = this.getStart();
	}

	public void resetCell() {
		for (int i = 0; i < this.maxLine; i++) {
			for (int j = 0; j < this.maxColumn; j++) {
				if (this.matrixCell[i][j].getCell() == "*")
					this.matrixCell[i][j].setCell(" ");
			}
		}
	}

	public Cell[][] getMatrixCell() {
		return matrixCell;
	}

	public Maze clone() {
		return new Maze(this.matrixCell, this.start, this.end, this.currCell, this.maxLine, this.maxColumn);
	}

	public String printMaze() {
		// my maze
		String result = "   ";
		for (int i = 0; i < maxColumn; i++) {
			if (i >= 9)
				result += " " + i;
			else
				result += " " + i + " ";
		}
		result += "\n";
		for (int i = 0; i < this.maxLine; i++) {
			if (i > 9)
				result += i + " ";
			else
				result += i + "  ";

			for (int j = 0; j < this.maxColumn; j++) {
				if (j == 0 && i == 0) {
					result += "[S]";
				} else if (this.getMatrixCell()[i][j].getCell() != "" && !this.getMatrixCell()[i][j].checkWall())
					result += "[" + this.getMatrixCell()[i][j].getCell() + "]";

				else
					result += "[■]";
			}
			result += "\n";
		}
		return result;
	}

	public String toString() {
		return this.currCell.toString();
	}
}
