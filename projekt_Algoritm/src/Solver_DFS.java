import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

/**
 * 
 * Denna algoritm kan beskrivas som:
 *
 * Så länge noden inte är vägg eller inte redan besökt, tryck värdet till
 * stacken. Om noden är densamma som slutpunkten då pop-up stacken, annars lägg
 * till noden i sökvägen och resursivt i alla fyra riktningarna.
 * 
 * Denna klass räknar också exekveringstid tills att hitta slut-punkt samt antal
 * steg.
 * 
 * @author Wissam Rashed
 * @author Rawand Alkilani
 * @version 2021-01-09
 * @version 2006-01-09
 *
 */

public class Solver_DFS extends Solver_abstract {
	/*
	 * Konstruktö, Labyrint för att lösa
	 */
	public Solver_DFS(Maze maze) {
		this.maze = maze;
		this.result = "";
		this.linkedNode = new Stack<Node<Maze>>();
		this.closedCell = new Stack<Cell>();
	}

	public String solver() {
		try {
			Boolean endIsFound = false;
			this.countNode = 0;
			this.pathSteps = 0;

			this.closedCell.clear();
			this.maze.startSolveMaze();

			this.linkedNode.clear();
			((Stack<Node<Maze>>) this.linkedNode).push(new Node<Maze>(this.maze));

			// räkna exekveringstid
			long startTime = System.currentTimeMillis();

			// algoritm som hittar huvudlösning
			while (!endIsFound) {
				if (this.linkedNode.isEmpty())
					break;

				else {
					Node<Maze> currentNode = ((Stack<Node<Maze>>) this.linkedNode).pop(); 
																								
					this.maze = (Maze) currentNode.getContent();
					Cell currState = this.maze.getCurrCell();

					if (currState.getLine() == this.maze.getEnd().getLine()
							&& currState.getCol() == this.maze.getEnd().getCol()) {
						Node<Maze> temp = new Node<Maze>(this.maze);
						temp.setParent(currentNode);
						((Stack<Node<Maze>>) this.linkedNode).push(temp);
						endIsFound = true;
					}

					else {
						LinkedList<Node<Maze>> nexts = this.getNextCell(); 
						if (!this.closedCell.contains(currState)) {
							((Stack<Cell>) this.closedCell).push(currState);
							currState.setCell("*");
						}

						Iterator<Node<Maze>> x = nexts.descendingIterator();

						while (x.hasNext()) {
							Node<Maze> temp = x.next();
							temp.setParent(currentNode);
							((Stack<Node<Maze>>) this.linkedNode).push(temp);
							this.countNode++;
						}
					}
				}
			}

			long endTime = System.currentTimeMillis();

			long time = endTime - startTime;

			this.result = " Depth-first search ";

			if (endIsFound) {
				this.maze.resetCell();
				Node<Maze> revertedTree = ((Stack<Node<Maze>>) this.linkedNode).pop();

				revertedTree = revertedTree.getParent().getParent();
				this.result += "Path: [" + this.maze.getEnd().toString() + "](End) && ";
				this.pathSteps++;

				while (revertedTree.hasParent()) {
					Maze temp = revertedTree.getContent();
					Cell state = temp.getCurrCell();

					if (!state.equals(this.maze.getEnd())) {
						this.maze.getMatrixCell()[state.getLine()][state.getCol()].setCell("*");
						this.pathSteps++;
					}
					revertedTree = revertedTree.getParent();
				}

				this.result += "[" + this.maze.getStart().toString() + "]" + "(Start) \n" + "Path length: "
						+ this.pathSteps + "\nNumber of nodes created: " + this.countNode + "\nExecution time: "
						+ time / 1000d + " seconds\n";
				this.result += this.maze.printMaze();
			} else {
				this.result += "Failed : The maze is unreachable.";
			}
		} catch (Exception e) {
			return "Fail File not found";
		}
		return this.result;
	}

	// flytta till en ny cell med hjälp av noder
	public LinkedList<Node<Maze>> getNextCell() {
		LinkedList<Node<Maze>> currentResult = new LinkedList<Node<Maze>>();

		// hämta 4 möjliga cell
		LinkedList<Maze> nexts = this.maze.getCurrCell().getNextCell();

		for (int i = 0; i < nexts.size(); i++) {
			Cell tempSq = nexts.get(i).getCurrCell();
			if (!this.closedCell.contains(tempSq)) {
				Node<Maze> tempNode = new Node<Maze>(nexts.get(i));
				currentResult.add(tempNode);
			}
		}

		return currentResult;
	}

	public String getResult() {
		return this.result;
	}

}
